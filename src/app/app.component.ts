import { Component } from "@angular/core";
import { FTService } from "./services/FT.service";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.css"]
})

export class AppComponent {

	/** Booléen permettant l'affichage conditionnel
	 * true = l'utilisateur effectue une recherche
	 * false = l'utilsateur consulte un document
	 */
	public search: Boolean = true;

	/** chaine de caractère entrée par l'utilistaeur dans la barre de recherche */
	public searchedValue: String = "";

	/** Attribut contenant les résultats de la recherche */
	public searchResults: any = [];

	/** Attribut contenant la table des matières du document sélectioné par l'utilisateur */
	public mapToC: any = [];

	constructor(private FTService: FTService) { }

	public ngOnInit(): void {
		/** Le code contenu dans le subscribe est exéuté à chaque fois que la valeur de l'observable auquel il souscrit est modifiée. */
		this.FTService.searchResultObservable.subscribe((response) => { this.searchResults = response["results"]; });
		this.FTService.tocObservable.subscribe((response) => this.mapToC = response);
	}


	public onSearch(): void {
		this.FTService.postMapsSearch(this.searchedValue);
		this.searchedValue = "";
	}


	public onView(mapId: String): void {
		this.search = false;
		this.FTService.mapId = mapId;
		this.FTService.getMap(mapId);
		this.FTService.getMapToC(mapId);
	}


	public getContentToDisplay(topicId: String): void {
		this.FTService.getTopicContent(this.FTService.mapId, topicId);
	}


	public onBack(): void {
		this.search = true;
		this.FTService.contentToDisplay = "";
		this.FTService.mapToDisplay = [];
	}
}
