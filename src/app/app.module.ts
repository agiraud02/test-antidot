import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { FTService } from "./services/FT.service";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { SanitizeHtmlPipe } from "./sanitizeHtml.pipe";

@NgModule({
  declarations: [
    AppComponent,
    SanitizeHtmlPipe
  ],
  imports: [
    BrowserModule,
	AppRoutingModule,
	HttpClientModule,
	NoopAnimationsModule,
	FormsModule
  ],
  providers: [
	  FTService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
