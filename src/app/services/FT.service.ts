import { Injectable } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

/**
 * Service qui gère la communication entre l'application angular et l'API
 */
@Injectable()
export class FTService {

	public FTPortalHost: String = "https://doc.antidot.net/";

	/**
	 * Les observables peuvent être "observés". Cela permet d'être notifié lorsque leur valeur change et de récupérer leur contenu.
	 * Il sont ici utilisés pour récupérer le résultat des requêtes de l'api et signaler au component angular que la réponse est arrivée et peut être traitée
	 */
	public suggestionObservable = new BehaviorSubject([]);
	public searchResultObservable = new BehaviorSubject([]);
	public mapTopicsObservable = new BehaviorSubject([]);
	public mapContentObservable = new BehaviorSubject([]);
	public tocObservable = new BehaviorSubject([]);

	/** Identifiant du document que l'utilisateur a sélectionné */
	public mapId: String = "";

	/** Document que l'utilisateur a sélectionné */
	public mapToDisplay: any = [];

	/** Contenu de la partie du document à afficher */
	public contentToDisplay: String;

	/** Identifiant de la partie du document sélectionée */
	public topicSelectedId: String;

	constructor(private httpClient: HttpClient) { }


	public getMapTopics(mapId: String): void {
		this.httpClient.get<any[]>(this.FTPortalHost + "api/khub/maps/" + mapId + "/topics").subscribe(
			(response) => {
				this.mapTopicsObservable.next(response);
			});
	}

	public getMap(mapId: String): void {
		this.httpClient.get<any[]>(this.FTPortalHost + "api/khub/maps/" + mapId ).subscribe(
			(response) => {
				this.mapToDisplay = response;
			});
	}

	public getMapToC(mapId: String): void {
		this.httpClient.get<any[]>(this.FTPortalHost + "api/khub/maps/" + mapId + "/toc" ).subscribe(
			(response) => {
				this.tocObservable.next(response);
				this.getTopicContent(this.mapId, response[0]["contentId"]);
			});
	}

	public getTopicContent(mapId: String, topicId: String): void {
		this.httpClient.get(this.FTPortalHost + "api/khub/maps/" + mapId + "/topics/" + topicId).subscribe(
			(response) => {
				this.topicSelectedId = response["id"];
				let metadata = response["metadata"];
				let sectionString: String = "";
				metadata.forEach(data => {
					if (!(data["key"] === "IMPORT_ID")) {
					sectionString = sectionString + "&" + data["key"] + "=" + data["values"][0];
					}
				});
				sectionString = sectionString.substr(1);
				this.getSection(sectionString);
			});
	}

	public getSection(code: String): void {
		this.httpClient.get(this.FTPortalHost + "api/khub/section/html?" + code , { "responseType": "text"}).subscribe(
			(response) => {
				this.contentToDisplay = response;
			});
	}

	public postSuggestion(inputUtilisateur: String): void {
		const jsonBody: String = JSON.stringify(
			{
				input: inputUtilisateur,
				contentLocale: "en-US",
				maxCount: 10
			}
		);
		this.httpClient.post<any[]>(this.FTPortalHost + "api/khub/suggest", jsonBody, { headers : { "Content-Type": "application/json"}}).subscribe(
			(response) => this.suggestionObservable.next(response)
		);
	}

	public postMapsSearch(inputUtilisateur: String): void {
		const jsonBody: String = JSON.stringify(
			{
				query: inputUtilisateur,
				"contentLocale": "en-US",
				"sort": [
				{
				"key": "ft:lastEdition",
				"order": "DESC"
				}
				],

				"facets": [
				{
				"id": "prodname",
				"maxDepth": 1
				}
				],
				"paging": {
				"page": 1,
				"perPage": 10
			}
		});
		this.httpClient.post<any[]>(this.FTPortalHost + "api/khub/maps/search", jsonBody, { headers : { "Content-Type": "application/json"}}).subscribe(
			(response) => this.searchResultObservable.next(response)
		);
	}
}
