# AntidotTest

## Outils & Versions

| Outils     | Versions |
| :--------- | :------: |
| Node       |  12.7.0  |
| npm        |  6.10.0  |
| Angular CLI   |  8.1.3   |

## Installation de l'environnement
### Node.js et npm
`sudo apt-get install nodejs npm`

### Angular
`npm install -g @angular/cli`

## Installation du projet
### Récupérer le code source
Décompresser l'archive envoyée par mail.

### Installer les dépendances du projet
A la racine du projet exécuter la commande suivante:  
`npm install`

### Lancer le serveur
A la racine du projet exécuter la commande suivante:  
`ng s -o`

## Résultat
Si vous ne souhaitez pas installer l'environnement pour tester le projet voici un aperçu de l'interface.
- L'utilisateur recherche le terme integration
![Page de recherche](./images/fluid1.png)
 - Affichage des 10 premiers résultats de la recherche
 ![Résultats](./images/fluid2.png)
 - L'utilisateur clique sur le bouton view du premier document
 ![Affichage document](./images/fluid3.png)
 - L'utilisateur sélectionne la partie du document à afficher de son choix
 ![Affichage partie spécifique](./images/fluid4.png)
 - A tout moment l'utilisateur peut revenir à la recherche en cliquant sur le bouton "Back to search".